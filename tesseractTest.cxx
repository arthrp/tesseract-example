#include <stdio.h>
#include <tesseract/baseapi.h>
#include <allheaders.h>

int main(int argc, char** argv){
    if(argc < 2){
        printf("Usage: tesseractTest [path to file]\n");
        exit(1);
    }

    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();

    if (api->Init(NULL, "eng")) {
        fprintf(stderr, "Could not initialize tesseract.\n");
        exit(1);
    }

    // Open input image with leptonica library
    Pix *image = pixRead(argv[1]);
    api->SetImage(image);
    // Get OCR result
    auto outText = api->GetUTF8Text();
    printf("OCR output:\n%s", outText);

    // Destroy used object and release memory
    api->End();
    delete api;
    delete [] outText;
    pixDestroy(&image);
    return 0;
}